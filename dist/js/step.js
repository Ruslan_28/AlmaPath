//step 

$(function ()
{
    $(".form-steps").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
         onStepChanged: (event, currentIndex, newIndex) =>
            {       


            },
    });



    $('.next').each( (i, item, next) => {
       $(item).click(() => {
            $(".form-steps").steps("next")
        })
    })


    $('.previous').each( (i, item, next) => {
       $(item).click(() => {
            $(".form-steps").steps("previous")
        })
    })





    const wrappers = $('.wrapper');
    wrappers.toArray().map((elem, index) => {

        $(elem).on('click', '.remove', function() {
            $(elem).find('.results').find($(elem).find('.element').not(':first').last().remove());
            if( $(elem).find('.results').html() === '' ) {
                $(elem).find('.remove').removeClass('remove-blc');
            }
        });

        $(elem).on('click', '.clone', function() {
            $(elem).find('.results').append($(elem).find('.element').first().clone());
            $(elem).find('.remove').addClass('remove-blc');
        });
    })


    


});




